const User = require("./../models/user.model");
const ChatMessage = require("./../models/chat.message.model");
const cron = require('node-cron');

function scheduleDeletingChat() {
    cron.schedule('0 0 1 * *', () => deletingMessages());
}

async function deletingMessages() {
    const moderator = await User.findOne({isModerator: true});

    let dateOfPreviousMonth = new Date();
    dateOfPreviousMonth.setMonth(dateOfPreviousMonth.getMonth()-1);

    const lastMessages = await ChatMessage.aggregate([
        { $sort: {createdAt: 1} },
        { $group: {_id: "$senderId", createdAt: {$first: "$createdAt"}} },
        { $match: {createdAt: {$lte: dateOfPreviousMonth}} },
        { $project: {_id: 1, createdAt: 0} }
    ]);

    const senderIds = [];

    lastMessages.forEach(lastMessage => {
         if (!lastMessage._id.equals(moderator._id)) {
             senderIds.push(lastMessage._id);
         }
    });

    await ChatMessage.deleteMany({$or: [{senderId: {$in: senderIds}}, {recipientId: {$in: senderIds}}]});
    // console.log(lastMessages);
}

module.exports = scheduleDeletingChat;