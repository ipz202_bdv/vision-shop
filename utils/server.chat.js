const {Server} = require("socket.io");
const User = require("./../models/user.model");
const ChatMessage = require("./../models/chat.message.model");

const chatServer = (server) => {
    const io = new Server(server);

    io.on("connection", async (socket) => {
        socket.on("newUser", async (userId) => {
            const user = await User.findById(userId);

            socket.join(userId);
        });
        socket.on("exitUser", (userId) => {
            socket.emit("update", userId)
        });
        socket.on("chat", async (message) => {
            const {userId, text, recipientId} = message;

            const user = await User.findById(userId);
            await ChatMessage.create({senderId: userId, recipientId, content: text});

            io.to(recipientId).emit("chat", message);
        });
    });
}

module.exports = chatServer;