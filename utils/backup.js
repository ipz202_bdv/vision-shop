const { spawn } = require('child_process');
const path = require('path');
const cron = require('node-cron');

function scheduleBackup() {
    cron.schedule('0 0 * * *', () => backupMongoDB());
}

function backupMongoDB() {
    const currentDate = new Date();

    const ARCHIVE_PATH = path.join(process.cwd(), 'public', 'backups', `${process.env.DB_NAME}-` +
    `${currentDate.getFullYear()}-${currentDate.getMonth()}-${currentDate.getDate()}--${currentDate.getHours()}-` +
    `${currentDate.getMinutes()}.gzip`);

    const child = spawn('mongodump', [
        `--db=${process.env.DB_NAME}`,
        `--archive=${ARCHIVE_PATH}`,
        '--gzip',
    ]);

    child.stdout.on('data', (data) => {
        console.log('stdout:\n', data);
    });
    child.stderr.on('data', (data) => {
        console.log('stderr:\n', Buffer.from(data).toString());
    });
    child.on('error', (err) => {
        console.log('error:\n', err);
    });
    child.on('exit', (code, signal) => {
        if (code) {
            console.log('Process exit with code:', code);
        }
        else if (signal){
            console.log('Process killed with signal:', signal);
        }
        else{
            console.log('Backup is successful!');
        }
    });
}

module.exports = scheduleBackup;