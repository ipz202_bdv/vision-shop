class AppError extends Error{
    constructor(message, statusCode, requiresView=true) {
        super(message);

        this.statusCode = statusCode;
        this.status = `${statusCode}`.startsWith('4') ? 'fail' : 'error';
        this.isOperational = true;
        this.requiresView = requiresView;

        Error.captureStackTrace(this, this.constructor);
    }
}

module.exports = AppError;