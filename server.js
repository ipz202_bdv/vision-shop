const express = require('express');
const path = require('path');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const app = require('./app');
const chatServer = require('./utils/server.chat');
const scheduleBackup = require('./utils/backup');
const scheduleDeletingChat = require('./utils/chatmessages.delete');

dotenv.config({ path: './.env'} );

mongoose.connect(process.env.DATABASE_LOCAL,{
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
}).then(() => console.log('DB connection successfully!'));


const port = process.env["PORT"] || 3000;
const server = app.listen(port,() => {
    console.log(`Listening on the port ${port}...`);
});

app.use(express.static(path.join(__dirname + '/public')));

chatServer(server);
scheduleBackup();
scheduleDeletingChat();