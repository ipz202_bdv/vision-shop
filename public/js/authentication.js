const login = async (email, password) => {
    try {
        const res = await axios({
            method: 'POST',
            url: 'http://127.0.0.1:3000/users/login',
            data: {
                email, password
            },
            headers: { 'axios-request': true }
        });

        if(res.data.status === 'success'){
            showAlert('success', 'Ви успішно увійшли до акаунту!');
            window.setTimeout(() => {
                location.assign('/');
            }, 1500);
        }
    } catch (err) {
        showAlert('error', err.response.data.message);
        console.log(err.response.data);
    }
};

const loginAdmin = async (email, password) => {
    try {
        const res = await axios({
            method: 'POST',
            url: 'http://127.0.0.1:3000/admin/',
            data: {
                email, password
            },
            headers: { 'axios-request': true }
        });
        if(res.data.status === 'success'){
            showAlert('success', 'Вітаємо у панелі адміна!');
            window.setTimeout(() => {
                location.assign('/');
            }, 1500);
        }
    } catch (err) {
        showAlert('error', err.response.data.message);
    }
};

const loginModerator = async (email, password) => {
    try {
        const res = await axios({
            method: 'POST',
            url: 'http://127.0.0.1:3000/moderator/',
            data: {
                email, password
            },
            headers: { 'axios-request': true }
        });
        if(res.data.status === 'success'){
            showAlert('success', 'Вітаємо у панелі модератора!');
            window.setTimeout(() => {
                location.assign('/');
            }, 1500);
        }
    } catch (err) {
        showAlert('error', err.response.data.message);
    }
};

const signUp = async (name, email, password, passwordConfirm) => {
    try {
        const res = await axios({
            method: 'POST',
            url: 'http://127.0.0.1:3000/users/signUp',
            data: {
                name, email, password, passwordConfirm
            },
            headers: { 'axios-request': true }
        });

        if(res.data.status === 'success'){
            showAlert('success', 'Вітаємо! Ви успішно зареєструвались!');
            window.setTimeout(() => {
                location.assign('/');
            }, 1500);
        }
    } catch (err) {
        showAlert('error', err.response.data.message);
    }
};

//type is either 'password' or 'data'
const updateUserSettings = async (data, type) => {
    try{
        const url = type === 'password' ? 'http://127.0.0.1:3000/users/updateMyPassword'
            : 'http://127.0.0.1:3000/users/updateMe'

        const res = await axios({
            method: 'PATCH',
            url,
            data,
            headers: { 'axios-request': true }
        });

        if(res.data.status === 'success'){
            showAlert('success', 'Дані було успішно змінено!');
            window.setTimeout(() => {
                location.href = window.location.href;
            }, 1500);
        }
    } catch (err) {
        showAlert('error', err.response.data.message);
    }
}

const loginForm = document.getElementById('login-form');
if(loginForm){
    loginForm.addEventListener('submit', async e => {
        e.preventDefault();
        const email = document.querySelector('.email').value;
        const password = document.querySelector('.password').value;
        await login(email, password);
    });
}

const loginAdminForm = document.querySelector('#login-admin-form');
if(loginAdminForm){
    loginAdminForm.addEventListener('submit', async e => {
        e.preventDefault();
        const email = document.querySelector('.email').value;
        const password = document.querySelector('.password').value;
        await loginAdmin(email, password);
    });
}

const loginModeratorForm = document.querySelector('#login-moderator-form');
if(loginModeratorForm){
    loginModeratorForm.addEventListener('submit', async e => {
        e.preventDefault();
        const email = document.querySelector('.email').value;
        const password = document.querySelector('.password').value;
        await loginModerator(email, password);
    });
}

const signUpForm = document.getElementById('signup-form');
if(signUpForm){
    signUpForm.addEventListener('submit', async e => {
        e.preventDefault();
        const name = document.querySelector('.name').value;
        const email = document.querySelector('.email').value;
        const password = document.querySelector('.password').value;
        const passwordConfirm = document.querySelector('.confirm-password').value;
        await signUp(name, email, password, passwordConfirm);
    });
}

const userForm = document.querySelector('.user-form');
if(userForm){
    userForm.addEventListener('submit', async e => {
        e.preventDefault();
        const name = document.querySelector('#user-name-id').value;
        const email = document.querySelector('#user-email').value;
        await updateUserSettings({name, email}, 'data');
    });
}

const userFormPassword = document.querySelector('.user-form-password');
if(userFormPassword){
    userFormPassword.addEventListener('submit', async e => {
        e.preventDefault();
        document.querySelector('.btn-user-pass').innerHTML = 'Оновлення';
        console.log(document.querySelector('.btn-user-pass').innerHTML);

        const passwordCurrent = document.querySelector('#current-pass').value;
        const password = document.querySelector('#new-pass').value;
        const passwordConfirm = document.querySelector('#confirm-pass').value;
        await updateUserSettings({ passwordCurrent, password, passwordConfirm }, 'password');

        document.querySelector('.btn-user-pass').innerHTML = 'Зберегти';

        passwordCurrent.value = '';
        password.value = '';
        passwordConfirm.value = '';
    })
}

const logOutButton = document.querySelector('.logout');
if(logOutButton){
    logOutButton.addEventListener('click', async () => {
        try{
            const res = await axios({
                method: 'GET',
                url:  'http://127.0.0.1:3000/users/logout',
                headers: { 'axios-request': true }
            });
            if(res.data.status === 'success'){
                location.assign('/');
            }
        } catch(err) {
            showAlert('error', 'Помилка виходу з акаунту!')
        }
    });
}

const hideAlert = () => {
    const el = document.querySelector('.alert');
    if (el) el.parentElement.removeChild(el);
};

const showAlert = (type, msg) => {
    hideAlert();
    const markup = `<div class="alert alert--${type}">${msg}</div>`;
    document.querySelector('body').insertAdjacentHTML('afterbegin', markup);
    window.setTimeout(hideAlert, 5000);
};