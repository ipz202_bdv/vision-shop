const resetPassword = async(token, password, passwordConfirm) => {
    try {
        const res = await axios({
            method: 'POST',
            url: `http://127.0.0.1:3000/users/resetPassword/${token}`,
            data: {
                password,
                passwordConfirm
            }
        });
        if(!res.data.status){
            showAlert('error', 'Помилка відновлення паролю');
        }
        if(res.data.status === 'success'){
            showAlert('success', 'Пароль успішно змінено!');
            window.setTimeout(() => {
                location.assign('/');
            }, 1500);
        }
    } catch (err) {
        showAlert('error', err.response.data.message);
    }
}

const buttonResetPassword = document.querySelector('.reset-password-button');
buttonResetPassword.addEventListener('click', async (e) => {
    e.preventDefault();
    const token = buttonResetPassword.getAttribute('token');
    const password = document.getElementsByName('password')[0].value;
    const passwordConfirm = document.getElementsByName('passwordConfirm')[0].value;
    await resetPassword(token, password, passwordConfirm);
})