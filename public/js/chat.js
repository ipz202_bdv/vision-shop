import { io } from "https://cdn.socket.io/4.4.1/socket.io.esm.min.js";

(() => {
    const chat = document.querySelector('.chat');
    const socket = io();

    const uname = document.querySelector('#user-name').innerHTML;
    const userId = document.querySelector('#user-id').value;
    const recipientId = document.querySelector('#recipient-id').value;
    const userAvatar = document.querySelector('.user-avatar').src;
    const createdAt = new Date().toLocaleString('ukr',
        {
            day: 'numeric',
            month: 'long',
            year: 'numeric',
            hour: 'numeric',
            minute: 'numeric'
        });

    let allMessages = chat.querySelectorAll('.chat-screen .text');
    allMessages.forEach(msg => {
        const messageText = msg.innerText;
        msg.innerHTML = messageText;
    });


    socket.emit('newUser', userId);

    chat.querySelector('.chat-screen #send-message').addEventListener('click', () => {
        let message = chat.querySelector('.chat-screen #message-input').value;

        if(message.length === 0) {
            return;
        }

        message = searchLink(message);
        // let startLinkIndexOf = message.indexOf('https://');
        // if (startLinkIndexOf === -1) {
        //     startLinkIndexOf = message.indexOf('http://');
        // }
        //
        // if (startLinkIndexOf !== -1) {
        //     const endLinkIndexOf = message.indexOf(' ' ,startLinkIndexOf);
        //     const link = message.slice(startLinkIndexOf, endLinkIndexOf);
        //     const linkHTML = `<a href="${link}">посилання</a>`;
        //     message = message.replace(link, linkHTML);
        // }

        renderMessage('my', {
            username: uname,
            text: message,
            createdAt: createdAt,
            avatar: userAvatar
        });
        socket.emit('chat', {
            username: uname,
            userId: userId,
            recipientId: recipientId,
            text: message,
            createdAt: createdAt,
            avatar: userAvatar
        });

        chat.querySelector('.chat-screen #message-input').value = "";
    });

    chat.querySelector('.chat-screen #exit-chat').addEventListener('click', () => {
        socket.emit('exitUser', userId);
    });

    socket.on('update', (update) => {
        renderMessage('update', update);
    });

    socket.on('chat', (message) => {
        renderMessage('other', message);
    });

    function renderMessage(type, message){
        let messageContainer = chat.querySelector('.chat-screen .messages');
        if(type === 'my'){
            let el = document.createElement('div');
            el.setAttribute('class', 'message my-message');
            el.innerHTML = `              
                <div>
                     <div class="avatar">
                          <img src="${message.avatar}" alt="User name">
                     </div>
                     <div class="name">You</div>
                     <div class="text">${message.text}</div>
                     <div class="time">${message.createdAt}</div>
                </div>
            `;
            messageContainer.appendChild(el);
        } else if(type === 'other'){
            let el = document.createElement('div');
            el.setAttribute('class', 'message other-message');
            el.innerHTML = `
                <div>                    
                     <div class="avatar">
                                    <img src="${message.avatar}" alt="User name">
                     </div>
                     <div class="name">${message.username}</div>
                     <div class="text">${message.text}</div>
                     <div class="time">${message.createdAt}</div>
                </div>
            `;
            messageContainer.appendChild(el);
        } else if(type === 'update'){
            let el = document.createElement('div');
            el.setAttribute('class', 'message other-message');
            el.innerText = message;
            messageContainer.appendChild(el);
        }
        messageContainer.scrollTop = messageContainer.scrollHeight - messageContainer.clientHeight;
    }

    function searchLink(message) {
        let startLinkIndexOf = message.indexOf('https://');
        if (startLinkIndexOf === -1) {
            startLinkIndexOf = message.indexOf('http://');
        }

        if (startLinkIndexOf !== -1) {
            let endLinkIndexOf = message.indexOf(' ' ,startLinkIndexOf);
            if (endLinkIndexOf === -1) {
                endLinkIndexOf = message.length;
            }
            const link = message.slice(startLinkIndexOf, endLinkIndexOf);
            const linkHTML = `<a href="${link}">посилання</a>`;
            message = message.replace(link, linkHTML);
        }

        return message;
    }
})()