const ratingAll = document.querySelectorAll('.comment-rating');
const starDiv = document.querySelectorAll('.stars');

let step = 1;
ratingAll.forEach((rating) => {
    const ratingValue = +rating.value;
    for(let i = step; i <= step + ratingValue - 1; i++){
    starDiv[i-1].classList = "fas fa-star";
    starDiv[i-1].style.color = "gold";
    }
    step += 5;
});
