const createOrder =  async (basketId) => {
    try {
        const res = await axios({
            method: 'POST',
            url: 'http://127.0.0.1:3000/products/basket/createOrder',
            data: {
                basketId
            }
        });

        if(res.data.status === 'success'){
            showAlert('success', 'Ваше замовлення успішно додано!');
            window.setTimeout(() => {
                location.assign('http://127.0.0.1:3000/products/basket');
            }, 1500);
        }
    } catch (err) {
        showAlert('error', err.response.data.message);
    }
};

const buttonBuy = document.querySelector('.btn-buy');
buttonBuy.addEventListener('click', async e => {
    e.preventDefault();
    const basketId = buttonBuy.getAttribute('basket-id');
    console.log(basketId);
    await createOrder(basketId);
});
