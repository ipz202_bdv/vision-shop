
const forgotPassword = async(email) => {
    try {
        const res = await axios({
            method: 'POST',
            url: 'http://127.0.0.1:3000/users/forgotPassword',
            data: {email}
        });

        if(res.data.status === 'success'){
            showAlert('success', 'Вам надійшло повідомлення на пошту для відновлення паролю!');
            window.setTimeout(() => {
                location.assign('/');
                }, 1500);
            }
        else{
            console.log('bad');
        }
        } catch (err) {
        showAlert('error', err.response.data.message);
    }
}


const buttonForgotPassword = document.querySelector('.forgot-password-button');
buttonForgotPassword.addEventListener('click', async (e) => {
    e.preventDefault();
    const email = document.getElementsByName('email')[0].value;
    console.log(email);
    await forgotPassword(email);
});


