const deleteProducts = async (productId, productType) => {
        const url = `http://127.0.0.1:3000/admin/${productType}/${productId}/delete`

        await axios({
            method: 'DELETE',
            url
        });
        location.href = window.location.href;
}

const deleteComments = async (commentId, productType) => {
    const url = `http://127.0.0.1:3000/admin/${productType}/${commentId}/deleteComment`

    await axios({
        method: 'DELETE',
        url
    });
    location.href = window.location.href;
}

const buttonDelete = document.querySelectorAll('.button-delete');
buttonDelete.forEach((button) => {
    button.addEventListener('click', e => {
        let target = e.currentTarget;
        const productId = target.getAttribute('product-id');
        const productType = target.getAttribute('product-type');

        deleteProducts(productId, productType);
    });
});

const buttonDeleteComment = document.querySelectorAll('.button-delete-comment');
buttonDeleteComment.forEach((button) => {
    button.addEventListener('click', async e => {
        let target = e.currentTarget;
        const commentId = target.getAttribute('comment-id');
        const addToBasket = document.querySelector('.button-add-to-basket');
        const productType = addToBasket.getAttribute('product-type');

        await deleteComments(commentId, productType);
    });
});
