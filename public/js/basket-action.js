const addProductToBasket = async (productId, productType, quantity) => {
    const url = `http://127.0.0.1:3000/products/${productType}/${productId}/basket`

    const res = await axios({
        method: 'POST',
        url,
        data: {
            quantity
        }
    });

    if(res.data.status === 'success'){
        const cart = document.querySelector('#buy-smth');
        cart.style.color = 'cornflowerblue';
        window.setTimeout(() => {
            cart.style.color = 'black';
        }, 2000);
    }
}

const updateQuantityItem = async (productId, productType, quantity, totalPrice) => {
    const url = `http://127.0.0.1:3000/products/${productType}/${productId}/basketUpdate`

    const res = await axios({
        method: 'PATCH',
        url,
        data: {
            quantity
        }
    });

    const totalPriceEl = totalPrice;

    if(res.data.status === 'success'){
        blockMinusButtons();

        totalPrice = totalPrice.innerHTML.split('грн');
        totalPrice[0] = res.data.totalPrice;
        totalPriceEl.innerHTML = totalPrice[0] + ' грн' + totalPrice[1];
        document.querySelector('.sub-total-price').innerHTML = res.data.subTotalPrice + ' грн';
    }
}

const deleteProductFromBasket = async (productId, productType) => {
    const url = `http://127.0.0.1:3000/products/${productType}/${productId}/basketDelete`

    await axios({
        method: 'DELETE',
        url
    });
    location.href = window.location.href;
}

const buttonAddToBasket = document.querySelectorAll('.button-add-to-basket');
buttonAddToBasket.forEach((button) => {
    button.addEventListener('click', async e => {
        e.preventDefault();
        let target = e.currentTarget;
        const productId = target.getAttribute('product-id');
        const productType = target.getAttribute('product-type');
        const quantityInput = document.querySelector('.quantity');
        const quantity = quantityInput === null ? 1 : quantityInput.value;
        console.log(quantity);
        await addProductToBasket(productId, productType, quantity);
    });
});

const buttonAddOrMinusQuantity = document.querySelectorAll('.plus-and-minus-quantity');
buttonAddOrMinusQuantity.forEach((button) => {
   button.addEventListener('click', async e => {
       let target = e.currentTarget;
       const productId = target.parentNode.getAttribute('product-id');
       const productType = document.querySelector('.product-type').innerHTML;
       let quantity = target.parentNode.querySelector('.quantity');


       if (target.classList.contains('plus')) {
           quantity.value++;
       } else {
           quantity.value--;
       }

       const totalPrice = target.parentNode.parentNode.querySelector('.total-price');
       await updateQuantityItem(productId, productType, quantity.value, totalPrice);
   });
});


const buttonProductDelete = document.querySelectorAll('.delete-from-cart');
buttonProductDelete.forEach((button) => {
    button.addEventListener('click', async e => {
        const target = e.target;
        const productId = target.getAttribute('product-id');
        const productType = target.parentNode.parentNode.querySelector('.product-type').innerHTML;
        await deleteProductFromBasket(productId, productType);
    });
});

const blockMinusButtons = () => {
    const minusButtons = document.querySelectorAll('.minus');
    const inputs = document.querySelectorAll('.quantity');

    for(let i = 0; i < inputs.length; i++){
        const quantityValue = inputs[i].value;
        const minusButton = minusButtons[i];

        minusButton.disabled = quantityValue <= 1;
    }
}

blockMinusButtons();

