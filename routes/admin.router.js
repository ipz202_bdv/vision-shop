const express = require("express");
const adminController = require("../controllers/admin.controller");
const authController = require("./../controllers/auth.controller");
const router = express.Router();

router
    .route('/')
    .get(adminController.getLoginForm)
    .post(adminController.loginAdmin);

router.get('/statistic-month', adminController.getStatisticForMonth);
router.get('/statistic-year', adminController.getStatisticForYear);
router.get('/statistic-day', adminController.getStatisticForDay);
router.get('/statistic-top5', adminController.getTop5);


router
    .route('/:type')
    .get(adminController.getAddForm)
    .post(authController.protect,
        authController.restrictTo('admin'),
        adminController.createNewProduct)

router.route('/:type/:id/update')
    .get(adminController.getEditForm)
    .post(authController.protect,
        authController.restrictTo('admin'),
        adminController.editProductInformation)


router.delete('/:type/:id/delete',
    authController.protect,
    authController.restrictTo('admin'),
    adminController.deleteProduct);

router.delete('/:type/:id/deleteComment',
    authController.protect,
    authController.restrictTo('admin'),
    adminController.deleteComment);


module.exports = router;