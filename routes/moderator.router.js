const express = require("express");
const moderatorController = require("../controllers/moderator.controller");
const authController = require("./../controllers/auth.controller");
const router = express.Router();

router
    .route('/')
    .get(moderatorController.getLoginForm)
    .post(moderatorController.loginModerator);

router.get('/chats',
    authController.protect,
    authController.restrictTo('moderator'),
    moderatorController.getChats);

router.get('/chats/:id',
    authController.protect,
    authController.restrictTo('moderator'),
    moderatorController.getChatWithUser);

module.exports = router;