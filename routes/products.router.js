const express = require("express");
const productsController = require("../controllers/products.controller");
const authController = require('../controllers/auth.controller');
const basketController = require('./../controllers/basket.controller');
const commentController = require('./../controllers/comment.controller');

const router = express.Router();
router.use(authController.isLoggedIn);

router.get('/basket', basketController.getBasketPage);

router.post('/basket/createOrder', basketController.createOrder);
router.get('/my-orders', basketController.getOrders);

router.get('/search', productsController.getProductsFromSearch);

router.post('/:type/:id/comments',
    authController.protect,
    authController.isLoggedIn,
    commentController.createComment);

router.get('/:type', productsController.getAllProducts);
router.get('/:type/:id', productsController.getProduct);

router.post('/:type/:id/basket', basketController.addProductToBasket);
router.patch('/:type/:id/basketUpdate', basketController.updateQuantityItem);
router.delete('/:type/:id/basketDelete', basketController.deleteProductFromBasket);

module.exports = router;