const express = require('express');
const userController = require('../controllers/user.controller');
const authController = require('../controllers/auth.controller')

const router = express.Router();
router.use(authController.isLoggedIn);

router.post('/login', authController.login);
router.get('/logout', authController.logout);
router.post('/signup', authController.validateEmail, authController.signup);

router.post('/forgotPassword', authController.forgotPassword);
router.post('/resetPassword/:token', authController.resetPassword);
router.get('/resetPassword/:token', authController.getResetPassword);

router.get('/login', authController.getLoginForm);
router.get('/signup', authController.getSignUpForm);
router.get('/forgotPassword', authController.getForgotPasswordForm);

router.use(authController.protect);

router.get('/me', authController.isLoggedIn, userController.getMe, userController.getUser);
router.patch('/updateMyPassword', authController.isLoggedIn, authController.updatePassword);
router.patch('/updateMe', authController.validateEmail, authController.isLoggedIn, userController.updateMe);
router.post('/updateAvatar', authController.isLoggedIn, userController.updateMyAvatar);
router.get('/chat', userController.getChatWithModerator);


module.exports = router;