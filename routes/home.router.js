const express = require('express');
const homeController = require('./../controllers/home.controller');
const authController = require("../controllers/auth.controller");
const router = express.Router();

router.use(authController.isLoggedIn);

router
    .route('/')
    .get(homeController.getHomePage);

module.exports = router;