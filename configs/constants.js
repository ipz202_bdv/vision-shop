module.exports = {
    productTypes: {
        glasses: 'glasses',
        lenses: 'lenses',
        cases: 'cases'
    },
    PHOTO_MIMETYPES: [
        'image/gif',
        'image/jpeg',
        'image/png',
        'image/jpg'
    ],
    ALLOWED_PRODUCT_FILTERS: {
        glasses:[
            'producer',
            'sex',
            'dioptres'
        ],
        lenses: [
            'producer',
            'typeOfLenses',
            'replacementMode'
        ],
        cases:[
            'producer',
            'material',
            'color'
        ]
    }
}
