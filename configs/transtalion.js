module.exports = {
    producer: 'Виробник',
    sex: 'Приналежність',
    dioptres: 'Діопртрії',
    typeOfLenses: 'Тип лінз',
    replacementMode: 'Термін заміни',
    material: 'Матеріал',
    color: 'Колір'
}