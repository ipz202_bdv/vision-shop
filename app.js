const express = require('express');
const morgan = require('morgan');
const expressFileUpload = require('express-fileupload')
const helmet = require('helmet');
const mongoSanitize = require('express-mongo-sanitize');
const xss = require('xss-clean');
const hbs = require('hbs');
const {engine} = require ('express-handlebars');
const bodyParser= require('body-parser');
const cookieParser = require('cookie-parser');


const AppError = require('./utils/app.error');
const globalErrorHandler = require('./controllers/error.controller');
const productsRouter = require('./routes/products.router');
const userRouter = require('./routes/user.router');
const homeRouter = require('./routes/home.router');
const adminRouter = require('./routes/admin.router');
const moderatorRouter = require('./routes/moderator.router');
const authController = require('./controllers/auth.controller');
const path = require("path");

const app = express();

app.engine("hbs", engine(
    {
        layoutsDir: "views/layouts",
        defaultLayout: "layout",
        extname: "hbs"
    }
))
app.set("view engine", "hbs");
hbs.registerPartials(__dirname + "/views/partials");


require('dotenv').config();


// Set security HTTP headers
//Serving static files
app.use(express.static(path.join(__dirname, 'public')));
app.use(
    helmet({
        contentSecurityPolicy: false,
    })
);

if(process.env.NODE_ENV === 'DEVELOPMENT'){
    //use morgan
    app.use(morgan('dev'));
}


// Body parser, reading data from the body into req.body
app.use(express.json({ limit: '10kb' }));
app.use(express.urlencoded({ extended: true, limit: '10kb'}));
app.use(cookieParser());

// Data sanitization against NoSQL query injection
app.use(mongoSanitize()); // it's forbids to use '$gt: ""' in email

// Data sanitization against XXS
app.use(xss()); // it's forbids to use html-elements in text like "name": "<div id='bad-code">Name</div>

app.use(bodyParser.urlencoded({extended: false}));
//app.use(bodyParser.json);

//The middleware
app.use((req, res, next) => {
    req.requestTime = new Date().toISOString();
    next();
});

app.use(expressFileUpload());

app.use(authController.isLoggedIn);

app.use('/', homeRouter);
app.use('/products', productsRouter);
app.use('/users', userRouter);
app.use('/admin', adminRouter);
app.use('/moderator', moderatorRouter);

app.all('*', (req, res, next) => {
    next(new AppError(`Шляху ${req.originalUrl} не існує!`, 404));
});

app.use(globalErrorHandler);

module.exports = app;