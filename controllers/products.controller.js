const Products = require('../models/product.model');
const Comment = require('../models/comment.model');
const catchAsync = require('../utils/catch.async');
const { ALLOWED_PRODUCT_FILTERS, productTypes } = require('../configs/constants');
const translation = require('../configs/transtalion');
const AppError = require('../utils/app.error');


exports.getAllProducts = catchAsync(async (req, res, next) => {

    const { type } = req.params;

    const {
        page = 1,
        sortBy = 'createdAt',  // поле, по якому налаштовується порядок
        order = 'asc', // from min to max - порядок
        ...filters
    } = req.query;
    const skip = (page - 1) * 6; // 6 - per page
    const orderBy = order === 'asc' ? -1 : 1;
    const sort = { [sortBy]: orderBy }; // об’єкт sort, в якому міститься поле з назвою, яка міститься в змінній sortBy
    const filterObject = getFilterObject(filters, type);

    const products = await Products.allProducts
        .find(filterObject)
        .limit(6)
        .skip(skip)
        .sort(sort)
        .lean();
    const count = await Products.allProducts.countDocuments(filterObject);
    const result = {
        data: products,
        page,
        limit: 6,
        pageCount: Math.ceil(count / 6)
    };

    const filtersItemsArray = [];
    let currentFilters = '';
    // цикл по властивостям об’єкта filters (по певному ключу)
    Object.keys(filters).forEach((key) => {
        if(!Array.isArray(filters[key])){
            currentFilters += '&' + key + '=' + filters[key];
        }
        else{
            filters[key].forEach((item) => {
                currentFilters += '&' + key + '=' + item;
            });
        }
    });

    for(let i = 0; i < ALLOWED_PRODUCT_FILTERS[type].length; i++){
        // віддає всі можливі унікальні значення для кожного фільтру
        await Products[type].distinct(ALLOWED_PRODUCT_FILTERS[type][i], {type}, function(error, results){
            const tmpFilter = [];
            results.forEach((result) => {
                const tmpObjForFilter = {}
                if(currentFilters.includes(`${ALLOWED_PRODUCT_FILTERS[type][i]}=${result}`)) {
                    tmpObjForFilter.checked = 'checked';
                    tmpObjForFilter.link = `/products/${type}?` + currentFilters.replace(
                        `&${ALLOWED_PRODUCT_FILTERS[type][i]}=${result}`,
                        '').slice(1);
                } else {
                    tmpObjForFilter.link = `/products/${type}?${ALLOWED_PRODUCT_FILTERS[type][i]}=${result}`
                        + currentFilters;
                }
                tmpObjForFilter.name = result;
                tmpObjForFilter.type = type;
                tmpFilter.push(tmpObjForFilter);
            });
            filtersItemsArray.push({ header: translation[ALLOWED_PRODUCT_FILTERS[type][i]], filters: tmpFilter });
        });
    }
    const pageLinks = [];
    for(let i = 1; i <= result.pageCount; i++){
        const tmpObject = {};
        tmpObject.number = i; //number of page on button
        if(!currentFilters){
            tmpObject.link = `/products/${type}` + `?page=${i}`
        }
        else{
            tmpObject.link = `/products/${type}?` + currentFilters + `&page=${i}`;
        }
        pageLinks.push(tmpObject);
        console.log(tmpObject.link);
    }

    res.render('catalog', { result, filtersItemsArray, type, pageLinks, title: '' });
});

const getFilterObject = (filters, type) => {
    const allowedFilters = ALLOWED_PRODUCT_FILTERS[type];
    const filterObject = {type};

    Object.keys(filters).forEach((key) => {
        if (allowedFilters.includes(key)) {
            filterObject[key] = filters[key];
        }
    });
    return filterObject;
}

exports.getProduct = catchAsync(async(req, res, next) => {
    let product
    try{
        product = await Products[req.params.type].findById(req.params.id).lean();
    } catch {
        return next(new AppError('Продукта з таким id не існує', 404));
    }

    if(!product) {
        return next(new AppError('Продукта з таким id не існує', 404));
    }

    const comments = await Comment.find( { product: req.params.id } ).lean();

    if(!Object.values(productTypes).includes(req.params.type)){
        return next(new AppError('Такого типу продукту не існує!', 400));
    }
    res.render(`${req.params.type}-details`, { product, comments });
});


exports.getProductsFromSearch = catchAsync(async (req, res, next) => {
    const products = await Products.allProducts.find({ name: { $regex: req.query.searchQuery, $options: 'i'} }).lean();
    const query = req.query.searchQuery;
    res.render('search-catalog', {
        products,
        query
    })
});

