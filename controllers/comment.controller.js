const  Comment = require('./../models/comment.model');
const  Products = require('../models/product.model');
const catchAsync = require('./../utils/catch.async')

exports.getAllComments = catchAsync(async (req, res, next) => {

    const comments = await Comment.find({ product: req.params.productId }).lean();

    res.json({
       status: 'success',
       results: comments.length,
        data: {
           comments
        }
    });
});

exports.createComment = catchAsync(async (req, res, next) => {

    req.body.product = req.params.id;
    if(!req.body.user){
        req.body.user = res.locals.user._id;
    }

   const newComment = await Comment.create(req.body);
    await calcRating(req.params.id, req.params.type);

   res.redirect(`/products/${req.params.type}/${req.params.id}`);
});

const calcRating = catchAsync(async (productId, productType) => {
    const comments = await Comment.find({ product: productId }).lean();

    if(comments.length){
        let commentRatingSum = 0;
        comments.forEach((comment) => {
            commentRatingSum += comment.rating;
        }) ;

        const ratingAverage = (commentRatingSum/comments.length).toFixed(1);
        await Products[productType].findByIdAndUpdate(productId, {ratingAverage})
    }
    else{
        const ratingAverage = 0;
        await Products[productType].findByIdAndUpdate(productId, {ratingAverage})
    }
});


exports.calcRating = calcRating;
