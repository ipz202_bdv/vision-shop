const catchAsync = require("../utils/catch.async");
const AppError = require("../utils/app.error");
const User = require('../models/user.model');
const Product = require('../models/product.model');
const Comment = require('./../models/comment.model');
const Order = require('../models/order.model');
const authController = require("../controllers/auth.controller");
const {PHOTO_MIMETYPES} = require("../configs/constants");
const {calcRating} = require("../controllers/comment.controller");
const {v4: uuidv4} = require("uuid");
const {productTypes} = require('../configs/constants');


exports.loginAdmin = catchAsync(async (req, res, next) => {
    const { email, password } = req.body;

    const cookieOptions = {
        expires: new Date(Date.now() + process.env.JWT_COOKIE_EXPIRES_IN * 24 * 60 * 60 * 1000),
        httpOnly: true
    };

    // Check if email and password exist
    if (!email || !password) {
        return next(new AppError('Будь ласка, заповніть ВСІ поля!', 400));
    }
    // Check if user exists && password is correct
    const user = await User.findOne({ email }).select('+password');

    if(!user.isAdmin){
        return next(new AppError('У Вас немає доступу до цієї сторінки!', 403));
    }

    if (!(await user.correctPassword(password, user.password))) {
        return next(new AppError('Неправильний пароль! Спробуйте ще раз', 401));
    }

    const token = authController.createToken(user._id);
    user.password = undefined;

    res.cookie('jwt', token, cookieOptions);

    res.json({
        status: 'success',
        token,
        data: {
            user
        }
    });
});

exports.getLoginForm = (req, res) => {
    res.render('login-admin', {
        title: 'Log into your account'
    });
};

exports.createNewProduct = catchAsync(async (req, res, next) => {

    if(!PHOTO_MIMETYPES.includes(req.files.imageCover.mimetype)){
        return next(new AppError('Цей файл має некоректний тип!', 400));
    }

    const fileExtensionImageCover = req.files.imageCover.name.split('.').pop();

    const imageCoverName = uuidv4() + '.' + fileExtensionImageCover;
    const imagesName = []

    await req.files.imageCover.mv(process.cwd() + '/public/img/'  + imageCoverName);

    if(!req.files.images.length){
        req.files.images = [req.files.images];
    }

    await req.files.images.forEach((image) => {
        if(!PHOTO_MIMETYPES.includes(image.mimetype)){
            return next(new AppError('Цей файл має некоректний тип!', 400));
        }

        const fileExtensionImages = image.name.split('.').pop();
        const imagesNameFile = uuidv4() + '.' + fileExtensionImages;
        imagesName.push(imagesNameFile);
        image.mv(process.cwd() + '/public/img/' + imagesNameFile);
    });


    if(await req.params.type === 'glasses'){
        if(!PHOTO_MIMETYPES.includes(req.files.sizeImage.mimetype)){
            return next(new AppError('Цей файл має некоректний тип! Можна додати тільки зображення', 400));
        }

        const fileExtensionSizeImage = req.files.sizeImage.name.split('.').pop();

        const sizeImageName = uuidv4() + '.' + fileExtensionSizeImage;
        await req.files.sizeImage.mv(process.cwd() + '/public/img/'  + sizeImageName);

        const newProduct = await Product[req.params.type].create({
            ...req.body,
            type: req.params.type,
            sizeImage: sizeImageName,
            imageCover: imageCoverName,
            images: imagesName
        });
    }
    else{
        const newProduct = await Product[req.params.type].create({
            ...req.body,
            type: req.params.type,
            imageCover: imageCoverName,
            images: imagesName
        });
    }

    res.redirect('/');
});

exports.getAddForm = (req,res) => {
    res.render(`add-${req.params.type}`,{
        title: 'Add product'
    });
}

exports.editProductInformation = catchAsync(async (req, res, next) => {
    if(req.files){
        if(req.files.sizeImage){
            if(!PHOTO_MIMETYPES.includes(req.files.sizeImage.mimetype)){
                return next(new AppError('Цей файл має некоректний тип! Можна додати тільки зображення', 400));
            }

            const fileExtensionSizeImage = req.files.sizeImage.name.split('.').pop();

            const sizeImageName = uuidv4() + '.' + fileExtensionSizeImage;
            await req.files.sizeImage.mv(process.cwd() + '/public/img/'  + sizeImageName);

            req.body.sizeImage = sizeImageName;
        }

        if(req.files.imageCover){
            if(!PHOTO_MIMETYPES.includes(req.files.imageCover.mimetype)){
                return next(new AppError('Цей файл має некоректний тип! Можна додати тільки зображення', 400));
            }

            const fileExtension = req.files.imageCover.name.split('.').pop();
            const imageCoverName = uuidv4() + '.' + fileExtension ;

            await req.files.imageCover.mv(process.cwd() + '/public/img/'  + imageCoverName);

            req.body.imageCover = imageCoverName;
        }

        if(req.files.images){
            const imagesName = [];

            if(!req.files.images.length){
                req.files.images = [req.files.images];
            }

            req.files.images.forEach((image) => {
                if(!PHOTO_MIMETYPES.includes(image.mimetype)){
                    return next(new AppError('Цей файл має некоректний тип! Можна додати тільки зображення', 400));
                }

                const fileExtensionImages = image.name.split('.').pop();
                const imagesNameFile = uuidv4() + '.' + fileExtensionImages;
                imagesName.push(imagesNameFile);
                image.mv(process.cwd() + '/public/img/' + imagesNameFile);
            });

            req.body.images = imagesName
        }

    }


    const product = await Product[req.params.type].findByIdAndUpdate(req.params.id, req.body,{
        ...req.body,
        type: req.params.type,
        new: true,
        runValidators: true
    });

    if(!product) {
        return next(new AppError('Жодного продукту з цим id не знайдено!', 404));
    }

    res.redirect('/');
});


exports.getEditForm = catchAsync(async (req,res) => {
    const product = await Product[req.params.type].findById(req.params.id).populate('comment').lean();

    res.render(`edit-${req.params.type}`,{
        title: 'Edit product',
        product
    })
});

exports.deleteProduct = catchAsync( async (req, res, next) => {
    const product = await Product[req.params.type].findByIdAndDelete(req.params.id);

    if(!product) {
        return next(new AppError('Жодного продукту з цим id не знайдено!', 404));
    }

    res.end();
});

exports.deleteComment = catchAsync( async (req, res, next) => {
    const comment = await Comment.findByIdAndDelete(req.params.id);
    await calcRating(comment.product, req.params.type);

    if(!comment) {
        return next(new AppError('Жодного коментаря з цим id не знайдено!', 404));
    }

    res.end();
});


exports.getStatisticForMonth = catchAsync( async (req, res, next) => {
    try {
        let dateOfCurrentMonth = new Date();
        let dateOfPreviousMonth = new Date();
        dateOfPreviousMonth.setMonth(dateOfPreviousMonth.getMonth()-1);

        const ordersPerMonth = await Order.find({'createdAt': {$gte: dateOfPreviousMonth, $lt: dateOfCurrentMonth}});

        const currentDate = dateOfCurrentMonth.getDate();
        const diffTime = Math.abs(dateOfCurrentMonth - dateOfPreviousMonth);
        const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

        const currentMonth = dateOfCurrentMonth.toLocaleString('eng', { month: 'long' });
        const previousMonth = dateOfPreviousMonth.toLocaleString('eng', { month: 'long' });

        let countOfOrders = [];
        for(let i = currentDate; i <= (diffDays + currentDate); i++){
            const statistic = ordersPerMonth.filter(order => {
                if(i > diffDays) {
                    return order.createdAt.getDate() === i - diffDays &&
                        order.createdAt.getMonth() === dateOfCurrentMonth.getMonth();
                } else {
                    return order.createdAt.getDate() === i &&
                        order.createdAt.getMonth() === dateOfPreviousMonth.getMonth();
                }
            }).length;

            if(i > diffDays) {
                countOfOrders[i] = {date: i - diffDays + ' ' + currentMonth, statistic: statistic}
            } else {
                countOfOrders[i] = {date: i + ' ' + previousMonth, statistic: statistic}
            }
        }
        countOfOrders.reverse();

        res.render('stat-month',{
            title: 'Statistic',
            countOfOrders, currentDate, currentMonth
        })
    } catch (e) {
        return next(new AppError(`${e}`, 400));
    }
});

exports.getStatisticForYear = catchAsync(async (req, res, next) => {
    try {
        let firstDateOfCurrentYear = new Date();
        firstDateOfCurrentYear.setMonth(0, 1);
        let firstDateOfNextYear = new Date();
        firstDateOfNextYear.setMonth(firstDateOfNextYear.getFullYear()+1);

        const ordersPerYear = await Order.find({'createdAt': {$gte: firstDateOfCurrentYear, $lt: firstDateOfNextYear}});

        const currentDate = new Date().getMonth();
        const month = firstDateOfCurrentYear.toLocaleString('eng', { month: 'long' });

        let countOfOrders = [];
        for(let i = 0; i <= currentDate ; i++){
            const statistic = ordersPerYear.filter(order => {
                return order.createdAt.getMonth() === i;
            }).length;

            const month = ["January","February","March","April","May","June","July","August","September","October","November","December"];
            countOfOrders[i] = {date: month[i], statistic: statistic}
        }


        res.render('stat-year',{
            title: 'Statistic',
            countOfOrders, month
        });


    } catch (e) {
        return next(new AppError(`${e}`, 400));
    }

});

exports.getStatisticForDay = catchAsync(async (req, res, next) => {
    try {
        const currentDate = new Date();
        const ordersPerDay = await Order.find({ 'createdAt': {$gte: currentDate.setHours(0, 0, 0)} })
            .populate({
                path: 'basketLines.productId',
                model: 'AllProducts'
            }).lean();


        let countGlassesProduct = 0;
        let countLensesProduct = 0;
        let countCasesProduct = 0;


        ordersPerDay.forEach(order => {
                order.basketLines.forEach(basketLine => {
                    if(basketLine.productId) {
                        switch (basketLine.productId.type) {
                            case productTypes.glasses:
                                countGlassesProduct += basketLine.quantity;
                                break;
                            case productTypes.lenses:
                                countLensesProduct += basketLine.quantity;
                                break;
                            case productTypes.cases:
                                countCasesProduct += basketLine.quantity;
                                break;
                        }
                    }
                });
        });

        res.render('stat-day',{
            title: 'Statistic',
            countGlassesProduct, countLensesProduct, countCasesProduct
        })
    } catch (e) {
        return next(new AppError(`${e}`, 400));
    }
});

exports.getTop5 = catchAsync(async (req, res, next) => {
   try {
        let orders = await Order.aggregate([
            { $project: {_id: 0, basketLines: 1} },
            { $unwind: '$basketLines' },
            { $group: {_id: '$basketLines.productId', count: {$sum: '$basketLines.quantity'}} },
            { $lookup: {from: 'products', localField: '_id', foreignField: '_id', as: 'product'} },
            { $sort: {count: -1} },
            { $limit: 5 }
        ]);

        orders = orders.filter((order) => {
           return order.product.length > 0;
        });
       res.render('stat-top-5',{
           title: 'Statistic',
           orders
       })
   } catch(e) {
       return next(new AppError(`${e}`, 400));
   }
});