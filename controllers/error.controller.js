const sendErrorDev = (err, req, res) => {
    if(req.headers['axios-request']){
        res.status(err.statusCode).json({
            status: err.status,
            error: err,
            message: err.message,
            stack: err.stack
        });
    }
    else{
        res.render('error', {
            title: 'Error',
            err
        })
    }
};


module.exports = (err, req, res, next) => {

    err.statusCode = err.statusCode || 500;
    err.status = err.status || 'error';

    if (process.env.NODE_ENV === 'DEVELOPMENT') {
        sendErrorDev(err, req, res);
    }
}