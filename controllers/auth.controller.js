const crypto = require('crypto');
const { promisify } = require('util');
const jwt = require('jsonwebtoken');
const User = require('../models/user.model');
const catchAsync = require('../utils/catch.async');
const AppError = require('../utils/app.error');
const sendEmail = require('../utils/email');
const emailValidator = require('email-validator');


const signToken = id => {
    return jwt.sign({ id }, process.env.JWT_SECRET, {
        expiresIn: process.env.JWT_EXPIRES_IN,
    });
};


const createToken = (userId) => {
    return signToken(userId);
}

const createAndSendToken = (user, statusCode, res) => {
    const token = signToken(user._id);

    const cookieOptions = {
        expires: new Date(Date.now() + process.env.JWT_COOKIE_EXPIRES_IN * 24 * 60 * 60 * 1000),
        httpOnly: true
    };

    res.cookie('jwt', token, cookieOptions);
    user.password = undefined;

    res.status(statusCode).json({
        status: 'success',
        token,
        data: {
            user
        }
    });
};

exports.signup = catchAsync(async (req, res, next) => {

    const { name ,email, password, passwordConfirm } = req.body;

    if (!name || !email || !password) {
        return next(new AppError('Будь ласка, заповніть ВСІ дані!', 400));
    }

    if (password.length < 8){
        return next(new AppError('Помилка! Пароль має бути не менше 8 символів!', 400));
    }

    if (password !== passwordConfirm){
        return next(new AppError('Помилка! Паролі не співпадають!', 400));
    }

    const userIsExist = await User.findOne({email});
    if(userIsExist){
        return next(new AppError('Помилка! Такий користувач вже існує!', 400));
    }

    const newUser = await User.create({
         name, email, password, passwordConfirm
    });

    createAndSendToken(newUser, 201, res);
});

exports.getSignUpForm = (req, res) => {
    res.render('signup', {
        title: 'Sign up'
    });
};


exports.login = catchAsync(async (req, res, next) => {
    const { email, password } = req.body;

    const cookieOptions = {
        expires: new Date(Date.now() + process.env.JWT_COOKIE_EXPIRES_IN * 24 * 60 * 60 * 1000),
        httpOnly: true
    };

    if (!email || !password) {
        return next(new AppError('Будь ласка, заповніть ВСІ дані!', 400));
    }

    const userIsExist = await User.findOne({email});
    if(!userIsExist){
        return next(new AppError('Помилка! Ви не зареєстровані!', 400));
    }

    const user = await User.findOne({ email }).select('+password');

    if (!(await user.correctPassword(password, user.password))) {
        return next(new AppError('Неправильний пароль! Спробуйте ще раз', 401));
    }


    const token = createToken(user._id);
    user.password = undefined;

    res.cookie('jwt', token, cookieOptions);
    res.json({
        status: 'success',
        token,
        data: {
            user
        }
    });
});

exports.getLoginForm = (req, res) => {
    res.render('login', {
        title: 'Log into your account'
    });
};

exports.logout = (req, res) => {
    res.cookie('jwt', 'loggedout', {
        expires: new Date(Date.now() + 10 * 1000),
        httpOnly: true
    });
    res.status(200).json({ status: 'success' });
};

exports.protect = catchAsync(async (req, res, next) => {
    // Getting token and check of it's there
    let token;
    if (req.headers.authorization && req.headers.authorization.startsWith('Bearer')) {
        token = req.headers.authorization.split(' ')[1];
    } else if (req.cookies.jwt){
        token = req.cookies.jwt;
    }

    if (!token) {
        return next(new AppError('Ви незаєрестровані! Будь ласка, ввійдіть в обліковий запис', 401));
    }

    // Verification token
    const decoded = await promisify(jwt.verify)(token, process.env.JWT_SECRET);

    // Check if user still exists
    const currentUser = await User.findById(decoded.id);
    if (!currentUser) {
        return next(new AppError('Користувача з таким токен більше не існує', 401));
    }

    // Check if user changed password after the token was issued
    if (currentUser.changedPasswordAfter(decoded.iat)) {
        return next(
            new AppError('Користувач щойно змінив пароль! Будь ласка, ввійдіть ще раз', 401)
        );
    }

    // GRANT ACCESS TO PROTECTED ROUTE
    req.user = currentUser;
    res.locals.user = currentUser.toObject();
    next();
});


exports.isLoggedIn = async (req, res, next) => {
    if (req.cookies.jwt) {
        try {
            // verify token
            const decoded = await promisify(jwt.verify)(
                req.cookies.jwt,
                process.env.JWT_SECRET
            );

            // Check if user still exists
            const currentUser = await User.findById(decoded.id);
            if (!currentUser) {
                return next();
            }

            // Check if user changed password after the token was issued
            if (currentUser.changedPasswordAfter(decoded.iat)) {
                return next();
            }

            // THERE IS A LOGGED IN USER
            res.locals.user = currentUser.toObject();
            return next();
        } catch (err) {
            return next();
        }
    }
    next();
};

exports.restrictTo = () => {
    return (req, res, next) => {
        if(!(req.user.isAdmin || req.user.isModerator)) {
            return next(new AppError('You don`t have permission to perform this action'), 403)
        }
        next();
    }
}

exports.forgotPassword = catchAsync(async (req, res, next) => {
    const user = await User.findOne({ email: req.body.email });
    if (!user) {
        return next(new AppError('Користувача з такою поштою не існує', 404));
    }

    // Generate the random reset token
    const resetToken = user.createPasswordResetToken();
    await user.save({ validateBeforeSave: false });

    // Send it to user's email
    const resetURL = `${req.protocol}://${req.get('host')}/users/resetPassword/${resetToken}`;

    const message = `Вітаємо Вас, ${user.name}! 
    Був запит на зміну пароля! Якщо ви не робили цей запит,  проігноруйте цей лист, будь ласка. 
    В іншому випадку натисніть це посилання, щоб змінити пароль: ${resetURL}`

    try {
        await sendEmail({
            email: user.email,
            subject: 'Посилання для відновлення паролю (дійсне протягом 10 хвилин)',
            message
        });

    } catch (err) {
        user.passwordResetToken = undefined;
        user.passwordResetExpires = undefined;
        await user.save({ validateBeforeSave: false});

        return next(new AppError(err), 500);
    }

    res.json({
        status: 'success',
        message: 'Токен надісланий на пошту!'
    });
});

exports.getForgotPasswordForm = (req, res) => {
    res.render('forgot-password', {
        title: 'Forgot password'
    });
};

exports.resetPassword = catchAsync(async (req, res, next) => {
    const hashedToken = crypto
        .createHash('sha256')
        .update(req.params.token)
        .digest('hex');

    const user = await User.findOne({
        passwordResetToken: hashedToken,
        passwordResetExpires: { $gt: Date.now() }
    });

    // If token has no expires and there is user, set the new password
    if (!user) {
        return next(new AppError('Усп! Токен не є дійсним або його час скінчився', 400));
    }

    if (req.body.password.length < 8){
        return next(new AppError('Помилка! Пароль має бути не менше 8 символів!', 400));
    }

    if (req.body.password !== req.body.passwordConfirm){
        return next(new AppError('Помилка! Паролі не співпадають!', 400));
    }

    user.password = req.body.password;
    user.passwordConfirm = req.body.passwordConfirm;
    user.passwordResetToken = undefined;
    user.passwordResetExpires = undefined;
    await user.save();


    createAndSendToken(user, 200, res);
});

exports.getResetPassword = (req, res, next) => {
    res.render('reset-password', {
        title: 'Reset password',
        token: req.params.token
    });
}

exports.updatePassword = catchAsync(async (req, res, next) => {
    const user = await User.findById(req.user.id).select('+password');

    if (!(await user.correctPassword(req.body.passwordCurrent, user.password))) {
        return next(new AppError('Ваш старий пароль не є правильним!', 401));
    }

    user.password = req.body.password;
    user.passwordConfirm = req.body.passwordConfirm;

    if (user.password !== user.passwordConfirm){
        return next(new AppError('Помилка! Паролі не співпадають!', 400));
    }

    if (user.password.length < 8){
        return next(new AppError('Помилка! Пароль має бути не менше 8 символів!', 400));
    }

    await user.save();

    createAndSendToken(user, 200, res);
});

exports.validateEmail = (req, res, next) => {
    if(emailValidator.validate(req.body.email)){
        next();
    }
    else{
        next(new AppError('Помилка! Некоректна пошта', 400));
    }
}


exports.createToken = createToken;