const Products = require('../models/product.model');
const Basket = require('../models/basket.model');
const Order = require('../models/order.model');
const catchAsync = require("../utils/catch.async");


exports.addProductToBasket = async (req, res, next ) => {
    const productId = req.params.id;
    const quantity = req.body.quantity;

    let basket = await Basket.Basket.findOne({ userId: res.locals.user._id }).lean();
    if(!basket){
        basket = (await Basket.Basket.create({ userId: res.locals.user._id })).toObject();
    }

    const basketLines = basket.basketLines;
    let quantityFinal = quantity;
    let basketLineToUpdateId, basketLineToUpdateIndex;
    basketLines.forEach((basketLine) => {
        // if this product us already in basket
        if(basketLine.productId.toString() === productId){
            basketLineToUpdateId = basketLine._id;
            quantityFinal = basketLine.quantity + (+quantity);
            basketLine.quantity = quantityFinal;
            basketLineToUpdateIndex = basketLines.indexOf(basketLine);
        }
    });
    const totalPrice = await calculateTotalPrice(productId, quantityFinal);

    //if we add product which doesn't already exist in basket (new product)
    if(!basketLineToUpdateId){
        const newBasketLine = (await Basket.BasketLine.create({productId, quantity: quantityFinal, totalPrice})).toObject();
        basketLines.push(newBasketLine);
    }
    else{
        await Basket.BasketLine.findByIdAndUpdate(basketLineToUpdateId,
            { quantity: quantityFinal, totalPrice });
        basketLines[basketLineToUpdateIndex].quantity = quantityFinal;
        basketLines[basketLineToUpdateIndex].totalPrice = totalPrice;

    }

    const subTotalPrice = await calculateSubTotalPrice(basketLines);
    const resFind = await Basket.Basket.findByIdAndUpdate(
        basket._id,
        { basketLines, subTotalPrice },
        {new: true});

    res.json({ status: 'success', subTotalPrice});
}

exports.updateQuantityItem = async (req,res, next) => {

    const  { quantity } = req.body; // read from number input
    const basket = await Basket.Basket.findOne({ userId: res.locals.user._id }).lean();
    const productId = req.params.id;


    let basketLineId;
    const basketLines = basket.basketLines;
    const totalPrice = await calculateTotalPrice(productId, quantity);
    basketLines.forEach((basketLine) => {
        if(productId === basketLine.productId.toString()){
            basketLineId = basketLine._id;
            basketLine.quantity = quantity;
            basketLine.totalPrice = totalPrice;
        }
    });


    const subTotalPrice = await calculateSubTotalPrice(basketLines);

    await Basket.BasketLine.findByIdAndUpdate(basketLineId, { quantity, totalPrice });
    await Basket.Basket.findByIdAndUpdate(basket._id, { basketLines, subTotalPrice });

    res.json({ status: 'success', totalPrice, subTotalPrice});
}

exports.deleteProductFromBasket = catchAsync( async (req, res, next) => {
    const basket = await Basket.Basket.findOne({ userId: res.locals.user._id }).lean();
    const productId = req.params.id;

    let basketLineId;
    const basketLines = basket.basketLines;

    for(let i = 0; i < basketLines.length; i++){
        if(productId === basketLines[i].productId.toString()){
            basketLineId = basketLines[i]._id;
            basketLines.splice(i, 1);
        }
    }

    const subTotalPrice = await calculateSubTotalPrice(basketLines);

    await Basket.Basket.findByIdAndUpdate(basket._id, { subTotalPrice, basketLines });
    await Basket.BasketLine.findByIdAndDelete(basketLineId);

    res.end();
});


exports.createOrder = catchAsync(async (req, res, next) => {
    const basketId = req.body.basketId;
    let basket = await Basket.Basket.findById(basketId);
    const basketLines = basket.basketLines;

    const newOrder = await Order.create({
        userId: basket.userId,
        basketLines: Array.from(basket.basketLines),
        subTotalPrice: basket.subTotalPrice,
        createdAt: new Date(),
        createdAtString: new Date().toLocaleString('ukr',
            {
                day: 'numeric',
                month: 'long',
                year: 'numeric',
                hour: 'numeric',
                minute: 'numeric'
            })
    });

    for (let i = 0; i < basketLines.length; i++){
        await Basket.BasketLine.findByIdAndDelete(basketLines[i]._id);
    }
    await Basket.Basket.findByIdAndDelete(basketId);

    res.json({status: 'success'});
});

exports.getOrders = catchAsync( async (req, res, next) => {
    let orders = await Order.find({ userId: res.locals.user._id }).lean();
    for(let i = 0; i < orders.length; i++){
        orders[i] = await fillBasketLines(orders[i]);
        if(orders[i].orderIsEmpty){
            orders.splice(orders[i-1], 1);
            --i;
        }
    }

    if(!orders.length){
        orders = null;
    }

    res.render('my-orders', {
        title: 'My orders',
        orders
    });
});

exports.getBasketPage = async (req, res) => {
    let basket = await Basket.Basket.findOne({ userId: res.locals.user._id }).lean();
    if(!basket){
        basket = (await Basket.Basket.create({ userId: res.locals.user._id })).toObject();
    }

    basket = await fillBasketLines(basket);
    res.render('basket', {
        title: 'Basket',
        basket
    });
};


const fillBasketLines = async (basket) => {
    for(let i = 0; i < basket.basketLines.length; i++){
        const productId = basket.basketLines[i].productId;
        basket.basketLines[i].product = await Products.allProducts.findById(productId).lean();

        if(!basket.basketLines[i].product){
            await Basket.BasketLine.findByIdAndDelete(basket.basketLines[i]._id.toString());
            basket.basketLines.splice(i, 1);
            --i;
        } else {
            const totalPrice = await calculateTotalPrice(productId, basket.basketLines[i].quantity);
            basket.basketLines[i].totalPrice = totalPrice;
        }
    }

    basket.orderIsEmpty = false;
    if(!basket.basketLines.length){
        await Order.findByIdAndDelete(basket._id);
        basket.orderIsEmpty = true;
    }

    const subTotalPrice = await calculateSubTotalPrice(basket.basketLines);
    await Order.findByIdAndUpdate(basket._id, {basketLines: basket.basketLines, subTotalPrice} );
    await Basket.Basket.findByIdAndUpdate(basket._id, {basketLines: basket.basketLines, subTotalPrice} );
    basket.subTotalPrice = subTotalPrice;

    return basket;
}


const calculateTotalPrice = async (productId, quantity) => {
    const product = await Products.allProducts.findById(productId).lean();
    return product.price * quantity;
}

const calculateSubTotalPrice = async (basketLines) => {
    let count = 0;
    basketLines.forEach((basketLine) => {
        count += basketLine.totalPrice;
    });

    return count;
}