const catchAsync = require("../utils/catch.async");
const AppError = require("../utils/app.error");
const User = require("../models/user.model");
const authController = require("../controllers/auth.controller");
const ChatMessage = require("../models/chat.message.model");
const Products = require("../models/product.model");

exports.loginModerator = catchAsync(async (req, res, next) => {
    const { email, password } = req.body;

    const cookieOptions = {
        expires: new Date(Date.now() + process.env.JWT_COOKIE_EXPIRES_IN * 24 * 60 * 60 * 1000),
        httpOnly: true
    };

    // Check if email and password exist
    if (!email || !password) {
        return next(new AppError('Будь ласка, заповніть ВСІ поля!', 400));
    }
    // Check if user exists && password is correct
    const user = await User.findOne({ email }).select('+password');

    if(!user.isModerator){
        return next(new AppError('У Вас немає доступу до цієї сторінки!', 403));
    }

    if (!(await user.correctPassword(password, user.password))) {
        return next(new AppError('Неправильний пароль! Спробуйте ще раз', 401));
    }

    const token = authController.createToken(user._id);
    user.password = undefined;

    res.cookie('jwt', token, cookieOptions);

    res.json({
        status: 'success',
        token,
        data: {
            user
        }
    });
});

exports.getLoginForm = (req, res) => {
    res.render('login-moderator', {
        title: 'Log into your account'
    });
};

exports.getChats = async (req,res) => {
    const userIds = await ChatMessage.find({senderId: {$nin: [res.locals.user._id]}}).distinct('senderId');
    const users = await User.find().where('_id').in(userIds).lean();

    const lastMessageSenderId = await ChatMessage.aggregate([
        { $match: { "senderId": {$in: userIds} } },
        { $sort: {createdAt: -1} },
        { $group: {_id: "$senderId", "createdAt": {$first: "$createdAtString"}} }
    ]);

    const userAndDate = [];

    lastMessageSenderId.forEach(sender => {
        const user = users.find(u => u._id.equals(sender._id));
        userAndDate.push({ user, createdAt: sender.createdAt });
    });

    userAndDate.sort((a, b) => {
        if (a.createdAt > b.createdAt) {
            return -1;
        } else {
            return 1;
        }
    });

    res.render(`moderator-chats`, {
        title: 'Chats',
        userAndDate
    });
}

exports.getChatWithUser = async (req, res, next) => {
    try{
        const recipientUser = await User.findById(req.params.id).lean();

        const messages = await ChatMessage.find({$or:[{senderId: recipientUser._id}, {recipientId: recipientUser._id}]})
            .sort({'createdAt': 1})
            .populate('senderId')
            .populate('recipientId').lean();


        res.render(`chat`, {
            title: 'Chats',
            messages,
            recipientUser
        });
    } catch {
        return next(new AppError('Користувача з таким id не існує', 404));
    }
}