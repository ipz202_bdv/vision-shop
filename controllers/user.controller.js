const User = require('../models/user.model');
const ChatMessage = require('../models/chat.message.model');
const catchAsync = require('../utils/catch.async');
const AppError = require('../utils/app.error');
const {PHOTO_MIMETYPES} = require("../configs/constants");
const {v4: uuidv4} = require("uuid");

const filterObj = (obj, ...allowedFields) => {
    const newObj = {};

    Object.keys(obj).forEach(el => {
       if(allowedFields.includes(el)) {
        newObj[el] = obj[el];
       }
    });

    return newObj
};

exports.getAllUsers = catchAsync(async (req, res) => {
    const users = await User.find();

    res.status(200).json({
        status: 'success',
        results: users.length,
        data: {
           users
       }
    });
});


exports.getMe = (req, res) => {
    req.params.id = req.user.id;
    res.render('my-account', {
        title: 'My account'
    });
};


exports.updateMe = catchAsync(async (req, res, next) => {
    if (req.body.password || req.body.passwordConfirm) {
        return next(new AppError('Цей шлях не для оновлення паролю. Будь ласка, використайте /updateMyPassword.', 400));
    }

    const filteredBody = filterObj(req.body, 'name', 'email');

    const updatedUser = await User.findByIdAndUpdate(req.user.id, filteredBody, {
        new: true,
        runValidators: true
    });

    res.json({
        status: 'success',
        data: {
            user: updatedUser
        }
    });
});

exports.updateMyAvatar = catchAsync(async (req, res, next) => {
    if(req.files) {
        if (req.files.avatar) {
            if (!PHOTO_MIMETYPES.includes(req.files.avatar.mimetype)) {
                return next(new AppError('Цей файл має некоректний тип! Можна додати тільки зображення', 400));
            }

            const fileExtension = req.files.avatar.name.split('.').pop();

            const avatarName = uuidv4() + '.' + fileExtension;
            await req.files.avatar.mv(process.cwd() + '/public/img/' + avatarName);

            req.body.avatar = avatarName;
        }
    }

    const updatedAvatar = await User.findByIdAndUpdate(req.user.id,{
        ...req.body,
        new: true,
        runValidators: true
    });

    res.redirect('/users/me');
});

exports.getUser = catchAsync(async (req, res, next) => {
    let user;
    try {
        user = await User.findById(req.params.id)
    } catch {
        return next(new AppError('Жодного юзера з цим id не знайдено!', 404));
    }

    if(!user) {
        return next(new AppError('Жодного юзера з цим id не знайдено!', 404));
    }

    res.render('my-account');
});

exports.getChatWithModerator = async (req, res) => {
    const messages = await ChatMessage.find({$or:[{senderId: res.locals.user._id}, {recipientId: res.locals.user._id}]})
        .populate('senderId').populate('recipientId').lean();
    const recipientUser = await User.findOne({isModerator: true}).lean();

    console.log(`fromUser: ${req.params.id}, ${res.locals.user._id}, ${recipientUser._id}`)

    res.render(`chat`, {
        title: 'Chats',
        messages,
        recipientUser
    });
}
