const mongoose = require('mongoose');

const basketLineSchema = new mongoose.Schema({
    productId: {
        type: mongoose.Schema.ObjectId,
        ref: 'AllProducts',
        required: true
    },
    quantity: {
        type: Number,
        default: 1,
        required: true
    },
    totalPrice: {
        type: Number,
        default: 0,
        required: true
    }
});

const basketSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: true
    },
    basketLines: {
        type: [basketLineSchema],
        default: [],
        required: true
    },
    subTotalPrice: {
        type: Number,
        default: 0,
        required: true
    }
});

const BasketLine = mongoose.model('BasketLine', basketLineSchema, 'basket');
const Basket = mongoose.model('Basket', basketSchema, 'basket');


module.exports = {BasketLine, Basket};