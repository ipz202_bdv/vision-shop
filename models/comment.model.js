const mongoose = require('mongoose');

const commentSchema = new mongoose.Schema({
   comment: {
       type: String,
       required: [true, `Comment can't be empty`]
   },
    rating: {
       type: Number,
        min: 1,
        max: 5
    },
    createdAt: {
       type: String,
        default: new Date().toLocaleString('ukr',
            {
                day: 'numeric',
                month: 'long',
                year: 'numeric',
                hour: 'numeric',
                minute: 'numeric'
            })
    },
    product: {
       type: mongoose.Schema.ObjectId,
        ref: 'AllProducts',
        required: [true, 'Comment must belong to a product']
    },
    user: {
       type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: [true, 'Comment must belong to a user']
    },
});


commentSchema.pre(/^find/, function (next) {
    this.populate({
       path: 'user',
       select: 'name photo avatar'
    });
   next();
});

const Comment = mongoose.model('Comment', commentSchema);

module.exports = Comment;


