const mongoose = require('mongoose');
const { productTypes } = require('../configs/constants');

const glassesSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Glasses must have a name'],
        unique: true,
        maxlength: [60, 'Glasses name must have less or equal then 60 characters'],
        minlength: [10, 'Glasses name must have more or equal then 10 characters'],
    },
    type: {
        type: String,
        required: [true, 'Glasses must have a type'],
        enum:Object.values(productTypes)
    },
    ratingAverage: {
      type: Number,
      min: [0, 'Rating must be above 0.0'],
      max: [5, 'Rating must be below 5.0'],
        default: 0
    },
    price: {
        type: Number,
        min: 0,
        required: [true, 'Glasses must have a price']
    },
    producer: {
        type: String,
        required: [true, 'Glasses must have a price']
    },
    dioptres: {
        type: String,
        required: [true, 'Glasses must have dioptres']
    },
    sex: {
        type: String,
        required: [true, 'Glasses must have sex']
    },
    materialOfGlassesFrames: {
        type: String,
        required: [true, 'Glasses must have materialOfGlassesFrames']
    },
    colorOfGlassesFrames: {
       type: String,
        required: [true, 'Glasses must have colorOfGlassesFrames']
    },
    description: {
        type: String,
        trim: true
    },
    sizeImage: {
        type: String,
    },
    imageCover: {
        type: String,
        required: [true, 'This product must have a cover image']
    },
    images: {
        type: [String],
        required: [true, 'This product must have a cover image']
    }
},
    {timestamps: true});

const lensesSchema = new mongoose.Schema({
   name: {
       type: String,
       required: [true, 'Glasses must have a name']
   },
    type: {
        type: String,
        required: [true, 'Lenses must have a type'],
        enum:Object.values(productTypes)
    },
    ratingAverage: {
        type: Number,
        min: [0, 'Rating must be above 0.0'],
        max: [5, 'Rating must be below 5.0'],
        default: 0
    },
    price: {
        type: Number,
        min: 0,
        required: [true, 'Glasses must have a price']
    },
    producer: {
        type: String,
        required: [true, 'Glasses must have a producer']
    },
    replacementMode: {
      type: String,
        required: true
    },
    diameter: {
        type: Number
    },
    typeOfLenses: {
        type: String
    },
    humidity: {
       type: Number
    },
    material: {
        type: String
    },
    amount: {
       type: Number
    },
    description: {
        type: String,
        trim: true //if we have " " at the beginning or in the end, it will cut
    },
    imageCover: {
        type: String,
        required: [true, 'This product must have a cover image']
    },
    images: {
        type: [String],
        required: [true, 'This product must have a cover image']
    }
},
    {timestamps: true});

const casesSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Case must have a name']
    },
    type: {
        type: String,
        required: [true, 'Cases must have a type'],
        enum:Object.values(productTypes)
    },
    ratingAverage: {
        type: Number,
        min: [0, 'Rating must be above 0.0'],
        max: [5, 'Rating must be below 5.0'],
        default: 0
    },
    price: {
        type: Number,
        min: 0,
        required: [true, 'Case must have a price']
    },
    producer: {
        type: String,
        required: [true, 'Glasses must have a producer']
    },
    material: {
        type: String
    },
    color: {
        type: String
    },
    measurements: {
        type: String
    },
    description: {
        type: String,
        trim: true //if we have " " at the beginning or in the end, it will cut
    },
    imageCover: {
        type: String,
        required: [true, 'This product must have a cover image']
    },
    images: {
        type: [String],
        required: [true, 'This product must have a cover image']
    }

},
    {timestamps: true});

const Glasses = mongoose.model('Glasses', glassesSchema, 'products');
const Lenses = mongoose.model('Lenses', lensesSchema, 'products');
const Cases = mongoose.model('Cases', casesSchema, 'products');
const AllProducts = mongoose.model('AllProducts', new mongoose.Schema({}), 'products');

module.exports = {glasses: Glasses, lenses: Lenses, cases: Cases, allProducts: AllProducts};
