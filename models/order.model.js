const mongoose = require('mongoose');
const { basketLineSchema } = require('../models/basket.model');

const ordersSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: true
    },
    basketLines: {
        type: {basketLineSchema},
        default: {},
        required: true
    },
    subTotalPrice: {
        type: Number,
        default: 0,
        required: true
    },
    createdAt: {
        type: Date,
        default: new Date()
    },
    createdAtString: {
        type: String,
        default: new Date().toLocaleString('ukr',
            {
                day: 'numeric',
                month: 'long',
                year: 'numeric',
                hour: 'numeric',
                minute: 'numeric'
            })
    }
});

const Order = mongoose.model('Order', ordersSchema);
module.exports = Order;