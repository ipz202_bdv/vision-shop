const mongoose = require("mongoose");

const chatMessageSchema = new mongoose.Schema({
    content: {
        type: String,
        required: true
    },
    senderId: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: true
    },
    recipientId: {
        type: mongoose.Schema.ObjectId,
        ref: 'User'
    },
    createdAt: {
        type: Date,
        default: new Date()
    },
    createdAtString: {
        type: String,
        default: new Date().toLocaleString('ukr',
            {
                day: 'numeric',
                month: 'long',
                year: 'numeric',
                hour: 'numeric',
                minute: 'numeric'
            })
    }
});

module.exports = mongoose.model("ChatMessage", chatMessageSchema);